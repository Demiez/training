import React from 'react';
import logo from './logo.svg';
import './App.css';

const HelloWorld = ({size,children}) => { //destructuring - only size
    console.log(size,children)
    let H = `h${size}`

    let newChildren = [];
    if (!children || !(children instanceof Array)) return  <div>OUPS</div>
    for (let child of children) {
        newChildren.push(typeof child === 'string' ? <div>{child}</div> : child)
    }
    return (
    <H style={{color: "red"}}>
        Hello
        {children && newChildren/*{children && children.map(text => typeof text === 'string' ? <div>{text}</div> : text)}*/}
    </H>
    )
};

function App() {
  return (
    <div className="App">
        {/*<HelloWorld size={1} something={"asd"}/>*/}
        {/*<HelloWorld size={2} something={"asd"}/>*/}
        {/*<HelloWorld size={3} something={"asd"}/>*/}
        {/*<HelloWorld size={4} something={"asd"}/>*/}
        {/*<HelloWorld size={5} something={"asd"}/>*/}
        {/*<HelloWorld size={6} something={"asd"}/>*/}
        <HelloWorld size={1} />
        <HelloWorld size={2} />
        <HelloWorld size={3} />
        <HelloWorld size={4} />
        <HelloWorld size={5} />
        <HelloWorld size={6}>
            <div>Superman</div>
            <div>Batman</div>
        </HelloWorld>
        <HelloWorld size={6} children={["Spiderman", "Beerman"]}/>
    </div>
  );
}

export default App;
