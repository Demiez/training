import React from 'react';
import './App.css';

class ChatInputs extends React.Component {
    state = {
        func: 'addMessage',
        nick: '',
        message: '',
    }
  render () {
        let nick = this.state.nick
        let message = this.state.message

      const checkNick = (e) => {
            this.setState({
                nick: e.target.value
            })
      }

      const checkMessage = (e) => {
          this.setState({
              message: e.target.value
          })
      }

      const sendObject = {
          func: this.state.func,
          nick: this.state.nick,
          message: this.state.message,
      }

    return (
        <div className='chatInputs'>
          <input className='nick'
                 placeholder='nick'
                 value={nick}
                 onChange={e => checkNick(e)}
          />
          <input className='message'
                 placeholder='message'
                 value={message}
                 onChange={e => checkMessage(e)}
          />
          <button onClick={() => this.props.onSend("http://students.a-level.com.ua:10012",
              sendObject)}
          >Send</button>
        </div>
    )
  }
}

class ChatHistory extends React.Component {
    state = {
        history: null,
    }

    componentDidMount() {
        setInterval(() => {
            this.props.onSend("http://students.a-level.com.ua:10012",
                {func: "getMessages", messageId: 0}).then((response) => {
                this.setState(
                    {history: response.data}
                    )
            })
        }, 3000)
    }

    render () {
        let history = this.state.history

        return (
            <div className='chat-history'>
                {(this.state.history) ? this.state.history.map(msg => <Message key={msg.timestamp.toString()}
                                                                               msg={msg}/>): ''}
            </div>
        )
    }

    static get defaultProps() {
        return {
            onSend() {
                throw new ReferenceError('onSend is not defined in ChatHistory')
            }
        }
    }
}

// class Blah extends React.Component {
//
//     static get defaultProps() {
//         return {
//             onSend() {
//                 throw new ReferenceError('onSend is not defined in Blah')
//             }
//         }
//
//
//     }
// }

class Message extends React.Component {
    render () {
        return (
            <div>
                {`${this.props.msg.nick} : ${this.props.msg.message}`}
            </div>
        )
    }
}
class App extends React.Component {
  render() {
      let jsonPost = (url, data) => {
          return new Promise((resolve, reject) => {
              var x = new XMLHttpRequest();
              x.onerror = () => reject(new Error('jsonPost failed'))

              x.open("POST", url, true);
              x.send(JSON.stringify(data))

              x.onreadystatechange = () => {
                  if (x.readyState == XMLHttpRequest.DONE && x.status == 200){
                      resolve(JSON.parse(x.responseText))
                  }
                  else if (x.status != 200){
                      reject(new Error('status is not 200'))
                  }
              }
          })
      }
    return (
        <div className="App">
          <ChatInputs onSend={(url, data) => {
              jsonPost(url, data)
            }
          }/>
          <ChatHistory onSend={(url, data) => {
              // jsonPost(url, data) - не работает (хотя верхний вариант для ChatInputs на отправку пашет)
              return new Promise((resolve, reject) => {
                  var x = new XMLHttpRequest();
                  x.onerror = () => reject(new Error('jsonPost failed'))

                  x.open("POST", url, true);
                  x.send(JSON.stringify(data))

                  x.onreadystatechange = () => {
                      if (x.readyState == XMLHttpRequest.DONE && x.status == 200){
                          resolve(JSON.parse(x.responseText))
                      }
                      else if (x.status != 200){
                          reject(new Error('status is not 200'))
                      }
                  }
              })
          }
          }/>
        </div>
    );
  }
}

export default App;
